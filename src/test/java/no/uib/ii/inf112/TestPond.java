package no.uib.ii.inf112;

import no.uib.ii.inf112.pond.Pond;
import no.uib.ii.inf112.pond.PondObject;
import no.uib.ii.inf112.pond.Position;
import no.uib.ii.inf112.pond.impl.Duck;
import no.uib.ii.inf112.pond.impl.Duckling;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class TestPond {
	Pond pond;
	Duck duck;
	Duckling duckling;

	@BeforeEach
	void setup() {
		pond = new Pond(100, 100);
		duck = new Duck(Position.create(2,3), 5);
		duckling = new Duckling(Position.create(6,1), 2);
	}

	@Test
	void TestStep() {
		for (int i = 0; i < 25; i ++) {
			pond.step();
		}
		assertEquals(2, pond.objs.size());
	}

	@Test
	void TestInitialOjectsInPond() {
		assertEquals(1, pond.objs.size());
	}

	@Test
	void TestAddDuck() {
		int objectsInPond = pond.objs.size();
		pond.add(duck);
		assertEquals(pond.objs.get(1), duck);
	}

	@Test
	void TestAddDuckling() {
		int objectsInPond = pond.objs.size();
		pond.add(duckling);
		assertEquals(pond.objs.get(1), duckling);
	}

	@Test
	void TestDuckXpos() {
		duck.moveTo(4,4);
		assertEquals(4, duck.getX());
	}

	@Test
	void TestDuckYpos() {
		duck.moveTo(4,4);
		assertEquals(4, duck.getY());
	}

	@Test
	void TestDucklingXPos() {
		duckling.moveTo(5,6);
		assertEquals(5, duckling.getX());
	}

	@Test
	void TestDucklingYPos() {
		duckling.moveTo(5,6);
		assertEquals(6, duckling.getY());
	}

}




