package no.uib.ii.inf112;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import no.uib.ii.inf112.impl.AlignerA;
import no.uib.ii.inf112.impl.AlignerB;
import no.uib.ii.inf112.impl.AlignerC;
import no.uib.ii.inf112.impl.AlignerD;
import no.uib.ii.inf112.impl.AlignerE;
import no.uib.ii.inf112.impl.AlignerF;
import no.uib.ii.inf112.impl.AlignerG;
import no.uib.ii.inf112.impl.AlignerH;

public abstract class TestAligner {
	TextAligner aligner;

	@Test
	void testCenter() {

		assertEquals(" A ", aligner.center("A", 3));
		assertEquals("  AA  ", aligner.center("AA", 6));
		assertEquals("  hello there  ", aligner.center("hello there", 15));
		assertEquals("  ABCD  ", aligner.center("ABCD", 8));
		assertEquals("AAAA", aligner.center("  AAAA", 4));
		assertEquals("  DETTE ER EN TEST  ", aligner.center("DETTE ER EN TEST  ", 20));




	}

	@Test
	void testFlushRight() {
		assertEquals("   A", aligner.flushRight("A", 4));
		assertEquals("  A", aligner.flushRight("A  ", 3));
		assertEquals("    HEI OG HOPP", aligner.flushRight("HEI   OG HOPP  ", 15));

	}

	@Test
	void testFlushLeft() {
		assertEquals("A   ", aligner.flushLeft("A", 4));
		assertEquals("A   ", aligner.flushLeft("   A", 4));
		assertEquals("HEI OG HOPP    ", aligner.flushLeft("   HEI OG HOPP ", 15));
		assertEquals("HEI OG HOPP          ", aligner.flushLeft("   HEI   OG   HOPP   ", 16));
	}

	@Test
	void testJustify() {
		assertEquals("hey   hey   hey", aligner.justify("hey hey hey", 15));
		assertEquals("hi   hi   hi", aligner.justify(" hi hi   hi ", 12));
		assertEquals("hei  hei  hei", aligner.justify("hei hei hei", 13));
		assertEquals("    HEI    HEI    ", aligner.justify("HEI HEI", 18));
	}
}

class TestAlignerA extends TestAligner {
	@BeforeEach
	void setup() {
		aligner = new AlignerA();
	}
}

class TestAlignerB extends TestAligner {
	@BeforeEach
	void setup() {
		aligner = new AlignerB();
	}
}

class TestAlignerC extends TestAligner {
	@BeforeEach
	void setup() {
		aligner = new AlignerC();
	}
}

class TestAlignerD extends TestAligner {
	@BeforeEach
	void setup() {
		aligner = new AlignerD();
	}
}

class TestAlignerE extends TestAligner {
	@BeforeEach
	void setup() {
		aligner = new AlignerE();
	}
}

class TestAlignerF extends TestAligner {
	@BeforeEach
	void setup() {
		aligner = new AlignerF();
	}
}

class TestAlignerG extends TestAligner {
	@BeforeEach
	void setup() {
		aligner = new AlignerG();
	}
}

class TestAlignerH extends TestAligner {
	@BeforeEach
	void setup() {
		aligner = new AlignerH();
	}
}